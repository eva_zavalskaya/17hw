﻿// 17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <math.h>
using namespace std;

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        cout << "\n" << x << " " << y << " " << z;
    }

    void Calc()
    {
        float a = sqrt(x * x + y * y + z * z);
        a = abs(a);
        cout << "\n" << a;
    }

private:
    double x;
    double y;
    double z;
};

class Calendar
{
public:
    Calendar() : Date(0), Month(0), Year(0)
    {}

    Calendar(int _Date, int _Month, int _Year) : Date(_Date), Month(_Month), Year(_Year)
    {}
    
    void Show()
    {
        cout << "\n Now it is " << "0" <<Date << "." << "0" << Month << "." << Year;
    }

private:
    int Date;
    int Month;
    int Year;
};

int main()
{
    Vector v(5,11,7);
    v.Show();
    cout << endl;
    v.Calc();
    cout << endl;
    Calendar f(3,9,2020);
    f.Show();
   
}


